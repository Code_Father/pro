import React, {Component} from 'react';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

class FormCategory extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const {values, handleChange} = this.props;

        return (<div style={styles.container}>
                <MuiThemeProvider>
                    <React.Fragment>
                        <TextField
                            hintText="Enter Category"
                            floatingLabelText="category"
                            onChange={handleChange("category")}
                            defaultValue={values.category}
                        />
                        <br/>
                        <RaisedButton
                            label="Continue"
                            primary={true}
                            style={styles.button}
                            onClick={this.continue}
                        />
                        <RaisedButton
                            label="Back"
                            primary={false}
                            style={styles.button}
                            onClick={this.back}
                        />
                    </React.Fragment>
                </MuiThemeProvider>
            </div>
        );
    }
}

const styles = {
    button: {
        margin: 15
    },
    container: {
        marginLeft: "40px"
    }
};

export default FormCategory;
