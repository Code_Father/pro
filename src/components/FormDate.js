import React, {Component} from 'react';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";

class FormDate extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const {values, handleChange} = this.props;

        return (<div style={styles.container}>
                <MuiThemeProvider>
                    <React.Fragment>
                        <TextField
                            hintText="Enter Your Date"
                            floatingLabelText="date"
                            onChange={handleChange("date")}
                            defaultValue={values.date}
                        />
                        <br/>
                        <RaisedButton
                            label="Continue"
                            primary={true}
                            style={styles.button}
                            onClick={this.continue}
                        />
                        <RaisedButton
                            label="Back"
                            primary={false}
                            style={styles.button}
                            onClick={this.back}
                        />
                    </React.Fragment>
                </MuiThemeProvider>
            </div>
        );
    }
}

const styles = {
    button: {
        margin: 15
    },
    container: {
        marginLeft: "40px"
    }
};

export default FormDate;
