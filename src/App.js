import React from 'react';
import './App.css';
import ConsultationPage from "./ConsultationPage/ConsultationPage";
import Header from "./Header/Header";

function App() {
    return (
        <div className="App">
            <Header/>
            <div className="flex-container">
                <ConsultationPage/>
            </div>
        </div>
    );
}

export default App;
